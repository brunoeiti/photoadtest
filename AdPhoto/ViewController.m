//
//  ViewController.m
//  AdPhoto
//
//  Created by BRUNO E TEIXEIRA on 07/02/22.
//

#import "ViewController.h"
#import "NetworkHelper.h"

@interface ViewController () <UITableViewDelegate,UITableViewDataSource,NetworkHelperDataDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tbImages;



@property(strong,nonatomic) NSArray * imageList;
@property(strong,nonatomic) NetworkHelper * networkHelper;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _imageList = @[];
    
    _networkHelper = [[NetworkHelper alloc]init];
    _networkHelper.delegate = self;
    [_networkHelper retrieveDataListImages];
}

#pragma mark NetworkHelper delegate
-(void)listImagesDataRetrieveFailed{
    
}
-(void)listImagesDataRetrieved:(NSArray *)imageList{
    self.imageList = imageList;
    [_tbImages reloadData];
}
-(void)imageDataRetrieveFailedforIndexPath:(NSIndexPath *)indexPath{
    
}
-(void)imageDataRetrieved:(UIImage *)image forIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [_tbImages cellForRowAtIndexPath:indexPath];
    if(cell != nil){
        UIImageView *imgPhoto = (UIImageView*)[cell viewWithTag:1];
        imgPhoto.image = image;
    }
}

#pragma mark TableView Delegate and data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _imageList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row % 4 == 3){
        return [self createAdCell:indexPath];
    }else{
        return [self createPhotoCell:indexPath];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row % 4 == 3){
        return 50;
    }else{
        return 406;
    }
}

#pragma mark TableviewCell helper functions
-(UITableViewCell *)createAdCell:(NSIndexPath*)indexPath{
    UITableViewCell * cell = [_tbImages dequeueReusableCellWithIdentifier:@"AdCell" forIndexPath:indexPath] ;
    
    return cell;
}
-(UITableViewCell *)createPhotoCell:(NSIndexPath*)indexPath{
    
    UITableViewCell * cell = [_tbImages dequeueReusableCellWithIdentifier:@"PhotoCell" forIndexPath:indexPath] ;
    //TODO: Adjust to get the correct object Index
    NSDictionary * dataObject = [_imageList objectAtIndex:indexPath.row];
    [_networkHelper retrieveImageForDataObject:dataObject forIndexPath:indexPath];
    
    return cell;
}

@end
