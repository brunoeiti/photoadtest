//
//  NetworkHelper.m
//  AdPhoto
//
//  Created by BRUNO E TEIXEIRA on 07/02/22.
//

#import "NetworkHelper.h"

@implementation NetworkHelper

-(void)retrieveDataListImages{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError *error;
        NSString *urlString =  @"https://picsum.photos/v2/list?page=2&limit=200";
        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:urlString]];
        NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if(error){
            NSLog(@"List Images Failed: %@",error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate listImagesDataRetrieveFailed ];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate listImagesDataRetrieved:json];
            });
        }
    });
    
}
-(void)retrieveImageForDataObject:(NSDictionary *)dataObject forIndexPath:(NSIndexPath*)indexPath{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlString = [dataObject objectForKey:@"download_url"];
        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:urlString]];
        if(data ==nil){
            NSLog(@"Image Download Failed: %@",urlString);

            [self.delegate imageDataRetrieveFailedforIndexPath:indexPath];
            return;
        }
        UIImage *image = [UIImage imageWithData:data];
        
        if(image == nil){
            NSLog(@"Image parse Failed: %@",urlString);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate imageDataRetrieveFailedforIndexPath:indexPath ];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate imageDataRetrieved:image forIndexPath:indexPath];
            });
        }
    });
}

@end
