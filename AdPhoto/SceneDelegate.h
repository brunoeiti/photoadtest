//
//  SceneDelegate.h
//  AdPhoto
//
//  Created by BRUNO E TEIXEIRA on 07/02/22.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

