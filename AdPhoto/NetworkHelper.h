//
//  NetworkHelper.h
//  AdPhoto
//
//  Created by BRUNO E TEIXEIRA on 07/02/22.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@protocol NetworkHelperDataDelegate <NSObject>

-(void)listImagesDataRetrieved:(NSArray*)imageList;
-(void)listImagesDataRetrieveFailed;

-(void)imageDataRetrieved:(UIImage*)image forIndexPath:(NSIndexPath*)indexPath;
-(void)imageDataRetrieveFailedforIndexPath:(NSIndexPath*)indexPath;

@end

@interface NetworkHelper : NSObject

@property(nonatomic,weak) NSObject<NetworkHelperDataDelegate> * delegate;

-(void)retrieveDataListImages;
-(void)retrieveImageForDataObject:(NSDictionary *)dataObject forIndexPath:(NSIndexPath*)indexPath;

@end

NS_ASSUME_NONNULL_END
